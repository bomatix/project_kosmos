from django.shortcuts import render, redirect
from django.views.generic import TemplateView, View
from django.views.generic.edit import CreateView
from .models import Question
from .forms import QuestionForm, UserRegistrationForm
from django.contrib.auth import authenticate, login
from . import helper_methods as hm

# Create your views here.
class QuestionCreateView(CreateView):
    model = Question
    fields = ['question']

def index(request):
    return render(request, 'questionAnswer/index.html')

def question_detail(request, question_path):
    q = Question.objects.filter(question__iexact=question_path.replace('_',' '))
    return render(request, 'questionAnswer/question_answer.html', {'question': q})

def get_question(request):
    if request.method == "POST":
        form = QuestionForm(request.POST)
        if form.is_valid():
            q = form.save(commit=False)
            # q.question = hm.delete_question_marks_from_string(q.question)
            q.save()
            return HttpResponseRedirect('../index/')
    else:
        form = QuestionForm()
    return render(request, 'questionAnswer/question_form.html', {"form": form})


class UserRegistrationFormView(View):
    form_class = UserRegistrationForm
    template_name = 'questionAnswer/registration_form.html'

    def get(self, request):
        form = self.form_class(None)
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = self.form_class(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user.set_password(password)
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            email = form.cleaned_data['email']
            user.save()
            return redirect('../index.html')
        return render(request, self.template_name, {'form': form})
