from django.urls import path, include
from . import views

urlpatterns = [
    path('postavi_pitanje/', views.get_question, name='QuestionCreateView'),
    path('index/', views.index, name='index'),
    path('<str:question_path>', views.question_detail, name='question_detail'),
    path('registruj_se/', views.UserRegistrationFormView.as_view(), name='UserRegistrationFormView')
]