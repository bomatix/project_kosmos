from django import forms
from django.contrib.auth.models import User
from .models import Question, MyUser

# class questionForm(forms.Form):
#     question = forms.CharField(label="Pitanje", max_length=250, 
#                         widget=forms.Textarea(attrs={'class': 'form-control question-area','rows':4, 'cols':15}),
#                         initial='Ovde unesite vaše pitanje?')


class QuestionForm(forms.ModelForm):
    question = forms.CharField(label="Pitanje", max_length=250, 
                        widget=forms.Textarea(attrs={'class': 'form-control question-area','rows':2, 'cols':15}),
                        initial='Ovde unesite vaše pitanje?')
    class Meta:
        model = Question
        fields = ['question']


class UserRegistrationForm(forms.ModelForm):
    password = forms.CharField(label='Lozinka', widget=forms.PasswordInput(attrs={'class': 'form-control form-field'}))
    username = forms.CharField(label='Username', widget=forms.TextInput(attrs={'class': 'form-control form-field'}))
    first_name = forms.CharField(label='Ime', widget=forms.TextInput(attrs={'class': 'form-control form-field'}))
    last_name = forms.CharField(label='Prezime', widget=forms.TextInput(attrs={'class': 'form-control form-field'}))
    email = forms.CharField(label='E-mail', widget=forms.TextInput(attrs={'class': 'form-control form-field'}))
    class Meta:
        model = MyUser
        fields = ['username', 'first_name', 'last_name', 'email', 'password']
