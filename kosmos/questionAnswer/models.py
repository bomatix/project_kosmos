from django.db import models
from django.urls import reverse
from . import helper_methods as hm
from django.contrib.auth.models import User
# Create your models here.


class MyUser(User):
    numberOfFollowers = models.IntegerField(default=0)

class Question(models.Model):
    question = models.CharField(max_length=500)
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    is_anonymous = models.BooleanField(default=False)

    def get_absolute_url(self):
        return reverse('questionAnswer:index', kwargs={'pk' : self.pk})

    def __str__(self):
        return str(self.question)

    

class Answer(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    #find a way to store blob files
    answer = models.TextField()
    upvotes = models.IntegerField(default=0)
    downvotes = models.IntegerField(default=0)
    is_best = models.BooleanField(default=False)
